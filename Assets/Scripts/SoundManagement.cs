﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagement : MonoBehaviour {

   

    //Sounds
    private AudioSource[] Sounds;
    private AudioSource Win;
    private AudioSource Lose;
    private AudioSource Music;
    private AudioSource Jump;
    
    // Use this for initialization
    void Start () {
        Sounds = GetComponents<AudioSource>();
        Jump = Sounds[0];
        Music = Sounds[1];
        Lose = Sounds[2];
        Win = Sounds[3];
        Music.Play();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayerJump()
    {
        Jump.Play();
    }
    public void Death()
    {
        Lose.Play();
    }
    public void Wingame()
    {
        Win.Play();
    }
}
