﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlaceHolderScript : MonoBehaviour {

    public InputField field;
    PlayerMovement playerScript;
    public GameObject player;
    public Text jumpIs, gameOverTxt;
    bool didInt;
    public int avInputs;
    public float timeout, setTimeout;
    public bool inputBool;
    public string inputString;

	// Use this for initialization
	void Start () {
        didInt = false;
        playerScript = player.GetComponent<PlayerMovement>();

        inputBool = false;
        inputString = "";
	}
    
	void LockInput(string text) //Aqui diferenciem entre diferents inputs
    {
        if (timeout > 0 || avInputs == 0) return;
        avInputs--;
        int numberCount = 0;
        bool point = false;
        Debug.Log("Entering text = "+text);
        for (int i = 0; i < text.Length; i++)
        {
            if (text[i] >= 48 && text[i] <= 57) numberCount++;
            if (text[i] == 46) point = true;
            if (text.ToLower() == "true" || text.ToLower() == "false") IsABool(text);
            else if (i == 0 && ((text[i] >= 65 && text[i] <= 89) || (text[i] >= 97 && text[i] <= 122))) IsAString(text);  //Mirem si es una frase
            else if (numberCount == text.Length) IsAInt(text);
            else if (numberCount == text.Length - 1 && point) IsAFloat(text);
            else Debug.Log("Text no diferenciat = " + i);
           
        }
        field.text = null;
        timeout = setTimeout;
    }
	// Update is called once per frame
	void Update () {
        if (avInputs <= 0) NoMoreInputs();
        else gameOverTxt.gameObject.SetActive(false);
        if (Input.GetKeyDown(KeyCode.Tab)) Restart();
        if (field.isFocused) playerScript.movementPlayer = false;           //Si s'esta utilitzant el input field el player no te control
        else playerScript.movementPlayer = true;
      
        field.onEndEdit.AddListener(delegate { LockInput(field.text); });   //Si s'introdueix text en el input field s'inicia lock input
        timeout -= Time.deltaTime;
    }

    void IsAString(string text)
    {
        jumpIs.text = "Jump is STRING with " + text;
        inputString = text;
    }

    void IsAInt(string text)
    {
        if (didInt) return;
        int n = int.Parse(text);
        if (n > playerScript.maxJumps) playerScript.avJumps = playerScript.maxJumps;
        else playerScript.avJumps = n;
        didInt = true;
        jumpIs.text = "Jump is INT with " + n;
    }

    void IsAFloat(string text)
    {
        float n = float.Parse(text);
        if (n > 2500) n = 2500;
        playerScript.jumpForce = n;
        jumpIs.text = "Jump is FLOAT with " + n;
    }

    void IsABool(string text)
    {
        if (text.ToLower() == "true")
        {
            inputBool = true;
            jumpIs.text = "Jump is BOOL with TRUE";
        }
        else
        {
            inputBool = false;
            jumpIs.text = "Jump is BOOL with FALSE";
        }
    }

    void Restart()
    {
        playerScript.transform.position = playerScript.initPos;
        playerScript.avJumps = playerScript.maxJumps;
        avInputs = 2;
    }
    void NoMoreInputs()
    {
        gameOverTxt.gameObject.SetActive(true);
    }
}
