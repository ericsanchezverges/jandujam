﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InputsLeftScript : MonoBehaviour {
    public GameObject inputField; 
    PlaceHolderScript pHolderScript;
    public Text txt;
    string a;
    // Use this for initialization
    void Start () {
        pHolderScript = inputField.GetComponent<PlaceHolderScript>();
	}
	
	// Update is called once per frame
	void Update () {
        txt.text = a + pHolderScript.avInputs;
	}
}
