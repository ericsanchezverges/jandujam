﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class JumpsLeftScript : MonoBehaviour {
    public GameObject player;
    public Text txt;
    PlayerMovement playerScript;
    string a;

	// Use this for initialization
	void Start () {
        playerScript = player.GetComponent<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        txt.text = a + playerScript.avJumps;
	}
}
