﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {


    public GameObject Code;
    public string theCode;

    public GameObject InputField;


	// Use this for initialization
	void Start () {
		if(theCode == null || theCode == "")
        {
            theCode = "patata";

        }
	}
	
	// Update is called once per frame
	void Update () {

	}


    private void OnTriggerStay(Collider other)
    {
        if(other.transform.tag == "Player")
        {

            Code.SetActive(true);
            //when jump and the code 
            if (InputField.GetComponent<PlaceHolderScript>().inputString == theCode ) {
                Code.GetComponent<TextMesh>().text = "GOOD CODE";
                Code.GetComponent<TextMesh>().color = Color.green;
                transform.GetComponent<MeshRenderer>().enabled = false;
                transform.GetComponent<MeshCollider>().enabled = false;
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {

            Code.SetActive(false);
            if (transform.GetComponent<MeshRenderer>().enabled)
            {
                transform.GetComponent<MeshRenderer>().enabled = true;
                transform.GetComponent<MeshCollider>().enabled = true;
            }
        }
    }
}
