﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour {

    public GameObject Audiomanager;

    private SoundManagement Soundscript;

	// Use this for initialization
	void Start () {
        Soundscript = Audiomanager.GetComponent<SoundManagement>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Player")
        {

            other.transform.position = other.GetComponent<PlayerMovement>().initPos;
            other.GetComponent<PlayerMovement>().avJumps = other.GetComponent<PlayerMovement>().maxJumps;
            Soundscript.Death();
        }
    }

}
