﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

    float alpha;
    Color color;
    public GameObject Audiomanager;

    private SoundManagement Soundscript;

    bool isInside;

	// Use this for initialization
	void Start () {
        alpha = 0;
        color = gameObject.GetComponent<SpriteRenderer>().color;
        Soundscript = Audiomanager.GetComponent<SoundManagement>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (isInside)
        {

            if(alpha >= 1)
            {
                Soundscript.Wingame();
                int nextscene = SceneManager.GetActiveScene().buildIndex + 1;
                //LEVEL COMPLETE
                SceneManager.LoadScene(nextscene);
                

            }

        }
        else if(!isInside && alpha > 0)
        {
            alpha -= 0.02f;
            if (alpha <= 0) alpha = 0;
        }


        Debug.Log(alpha);
        gameObject.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, alpha);

    }

    private void OnTriggerStay(Collider other)
    {
        if(other.transform.tag == "Player")
        {

            alpha += 0.02f;
            isInside = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {

        isInside = false;

    }
}
