﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviours : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "MovingPlatform")
        {
            gameObject.transform.parent = collision.transform;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "MovingPlatform")
        {
            gameObject.transform.parent = null;
        }
    }
}
