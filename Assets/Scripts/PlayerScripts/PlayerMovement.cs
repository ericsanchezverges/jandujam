﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public float baseSpeed;
    public float speed;
    public float maxSpeed;
    public float accel;
    public float sprintInicial;
    public float jumpForce;
    public Vector3 initPos;
    Transform playerTransform;
    bool jump, grownded, grownded2;
    public bool movementPlayer;
    public int maxJumps, avJumps;


    public GameObject Objectsound;

    private SoundManagement Soundscript;


    void Start()
    {
        initPos = rb.position;
        baseSpeed = speed;
        rb = GetComponent<Rigidbody>();
        playerTransform = GetComponent<Transform>();
        initPos = transform.position;
        movementPlayer = true;
        Soundscript = Objectsound.GetComponent<SoundManagement>();
    }

    void accelSpeed()
    {
        if (speed == baseSpeed) speed += sprintInicial; //He provat de pujar directament la velocitat a l'inici de l'sprint per fer-lo sentir millor
        if (speed < maxSpeed)
        {
            speed += accel * Time.deltaTime; //Segurament el Time.deltaTime sobra pero aixi es pot posar un nombre prou decent a l'acceleracio
        }
        else speed = maxSpeed;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && (grownded || grownded2) && avJumps > 0) //Trigger de el jump
        {
            jump = true;
            avJumps--;
            
        }
        Soundscript.PlayerJump();


        RaycastHit hitDown;
        if (Physics.Raycast(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), transform.TransformDirection(Vector3.down), out hitDown, Mathf.Infinity))
        {
            if (hitDown.distance > 0.5) grownded = false;
            else grownded = true;
            Debug.Log("Grownded = " + grownded);
        }
        RaycastHit hitDown2;
        if (Physics.Raycast(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), transform.TransformDirection(Vector3.down), out hitDown2, Mathf.Infinity))
        {
            if (hitDown2.distance > 0.5) grownded2 = false;
            else grownded2 = true;
            Debug.Log("Grownded2 = " + grownded2);
        }

    }

    void FixedUpdate()
    {
        if (!movementPlayer) return;
        float xInput = Input.GetAxisRaw("Horizontal");
        if (xInput != 0)
        {
            //transform.position += new Vector3(xInput, yInput, 0) * speed * Time.deltaTime * 0.70710678118f; //Valor aproximat del sinus de 45º, per fer que la velocitat sigui la mateixa en diagonal
            rb.MovePosition(transform.position + new Vector3(xInput, 0, 0) * speed * Time.deltaTime * 0.70710678118f);
        }
        else
        {
            //transform.position += new Vector3(xInput, yInput, 0) * speed * Time.deltaTime;
            rb.MovePosition(transform.position + new Vector3(xInput, 0, 0) * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.LeftShift) && (xInput != 0))
        { //Si et pares es deixa de carregar l'sprint i torna a la velocitat inicial
            accelSpeed();
        }
        else speed = baseSpeed; //No cal fer GetKeyUp
        if (jump)
        {
            rb.velocity = new Vector3(0, jumpForce * Time.deltaTime, 0);
            jump = false;
        }

    }
    
}



