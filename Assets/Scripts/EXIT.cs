﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EXIT : MonoBehaviour {

    bool InputExit = false;

    private void Start()
    {
        InputExit = false;
    }

    private void Update()
    {
        InputExit = Input.GetKeyDown(KeyCode.Escape);

        if (InputExit)
        {
            Application.Quit();
        }
    }


}
