﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {

    public bool expectedbool;
    public GameObject InputField;


    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player") {
            if (expectedbool == InputField.GetComponent<PlaceHolderScript>().inputBool)
                transform.GetComponent<MovingPlatform>().enabled = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
                transform.GetComponent<MovingPlatform>().enabled = false;
        }
    }
}
