﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    private GameObject MenuJuego;

	// Use this for initialization
	void Start () {
        MenuJuego = GameObject.Find("Menu");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        SceneManager.LoadScene(2);
    }

    public void Aboutus()
    {
        SceneManager.LoadScene("Aboutus");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
