﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuboRotation : MonoBehaviour {
    private BoxCollider Collcubo;

	// Use this for initialization
	void Start () {
        Collcubo = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        Vector3 midaCubo = Collcubo.bounds.size;
        float gradosCubo = Mathf.Atan2(midaCubo.y, midaCubo.x);
        float giroCubo = Mathf.Cos(gradosCubo);
        transform.Rotate(0, giroCubo, 0);
	}
    
}
